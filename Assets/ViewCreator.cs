﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using UnityEngine;
using UnityEngine.UI;
using test;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using ZipFile = System.IO.Compression.ZipFile;

public class ViewCreator : MonoBehaviour
{
    public Transform dlPane;
    public Transform playPane;
    public Transform noInternetPane;
    public Transform contentPane;
    public Transform playContentPane;
    public GameObject panePrefab;
    public GameObject playPanePrefab;
    public InputField searchField;
    public Button searchButton;
    public Sprite unstaticPlaySprite;

    private static GameObject SplayPanePrefab;
    private static Sprite playSprite;
    private static Transform SdlPane;
    private static Transform SplayContentPane;
    private static Transform SplayPane;

    private void Start()
    {
        SdlPane = dlPane;
        SplayPane = playPane;
        SplayPanePrefab = playPanePrefab;
        SplayContentPane = playContentPane;

        playSprite = unstaticPlaySprite;
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.Log("Error. Check internet connection!");

            noInternetPane.gameObject.SetActive(true);
        }
        else
            noInternetPane.gameObject.SetActive(false);

        Debug.Log(Application.persistentDataPath);

        searchButton.onClick.AddListener(Search);
    }

    private bool granted = false;

    private void Update()
    {
        if (!granted)
        {
            Debug.Log("====================================");
            var result = AndroidRuntimePermissions.RequestPermission("android.permission.INTERNET");
            granted = result == AndroidRuntimePermissions.Permission.Granted;

            Debug.Log(granted);

            noInternetPane.gameObject.SetActive(!granted);
            Debug.Log("====================================");
        }
    }

    private void Search()
    {
        var text = searchField.text;

        if (string.IsNullOrEmpty(text) || string.IsNullOrWhiteSpace(text))
            return;

        for (var i = 0; i < contentPane.childCount; i++)
        {
            Destroy(contentPane.GetChild(i).gameObject);
        }

        Debug.Log("======== start =======");

        StartCoroutine(Request());
    }

    private IEnumerator Request()
    {
        var request = UnityWebRequest.Get($"https://api.gatari.pw/beatmaps/search?q={searchField.text}&p=1&m=0&r=0");
        yield return request.SendWebRequest();

        while (!request.isDone)
        {
            yield return new WaitForFixedUpdate();
        }

        var html = DownloadHandlerBuffer.GetContent(request);

        /*var data = response.GetResponseStream();
        string html;

        Debug.Log("===== get =====");

        using (var sr = new StreamReader(data ?? throw new NullReferenceException("Returned data is null")))
        {
            html = sr.ReadToEnd();
        }*/

        var js1 = new DataContractJsonSerializer(typeof(SearchResult));
        var ms1 = new MemoryStream(Encoding.UTF8.GetBytes(html));

        var searchResult = (SearchResult) js1.ReadObject(ms1);

        Debug.Log("==========");

        Debug.Log(searchResult.code + " " + searchResult.result.Length);

        var position = new Vector3(0, -150, 0);

        foreach (var beatMap in searchResult.result)
        {
            var bmPane = Instantiate(panePrefab, contentPane, true);

            bmPane.transform.GetChild(1).GetComponent<Text>().text = beatMap.title;
            bmPane.transform.localPosition = position;

            bmPane.transform.GetChild(4).GetComponent<MusicHandler>().beatmapId = beatMap.beatmapset_id;

            //var pos = bmPane.transform.localPosition;
            //pos.y += 180 * (searchResult.result.Length - 3);
            //bmPane.transform.localPosition = pos;

            StartCoroutine(SetImage(beatMap.beatmapset_id, bmPane.transform.GetChild(0).GetComponent<Image>()));

            var mapPath = $"{Application.persistentDataPath}/{beatMap.beatmapset_id}";

            if (Directory.Exists(mapPath))
            {
                bmPane.transform.GetChild(7).GetComponent<Button>().image.sprite = playSprite;
                bmPane.transform.GetChild(5).GetComponent<Image>().sprite = playSprite;

                var rect = bmPane.transform.GetChild(6).GetComponent<RectTransform>();

                var Rpos = rect.localPosition;
                var Rsize = rect.sizeDelta;

                Rpos.x = 283;
                Rsize.x = 50;
                Rsize.y = 50;

                rect.localPosition = Rpos;
                rect.sizeDelta = Rsize;

                bmPane.transform.GetChild(7).GetComponent<Button>().onClick.AddListener(() =>
                {
                    launchPlay($"{Application.persistentDataPath}/{beatMap.beatmapset_id}");
                });
            }
            else
            {
                Coroutine coroutine = null;
                bmPane.transform.GetChild(7).GetComponent<Button>().onClick.AddListener(() =>
                {
                    if (currentlyDling.Contains(beatMap.beatmapset_id) && coroutine != null)
                    {
                        StopCoroutine(coroutine);

                        var rect = bmPane.transform.GetChild(6).GetComponent<RectTransform>();

                        var Rpos = rect.localPosition;
                        var Rsize = rect.sizeDelta;

                        Rpos.x = 243;
                        Rsize.x = 0;

                        rect.localPosition = Rpos;
                        rect.sizeDelta = Rsize;
                    }
                    else
                        coroutine = StartCoroutine(DownloadBeatMap(bmPane, beatMap.beatmapset_id,
                            bmPane.transform.GetChild(6)));
                });
            }

            position.y -= 310;
        }

        var pos = contentPane.GetComponent<RectTransform>().position;
        pos.y = -99999;
        contentPane.GetComponent<RectTransform>().position = pos;
    }

    private List<int> currentlyDling = new List<int>();

    private static IEnumerator DownloadBeatMap(GameObject bmPane, int beatMapBeatmapsetId, Transform dlImage)
    {
        dlImage.GetComponent<Image>().color = Color.green;

        var url =
            $"https://bloodcat.com/osu/s/{beatMapBeatmapsetId}"; //$"https://beatconnect.io/b/{beatMapBeatmapsetId}";
        var www = UnityWebRequest.Get(url);
        www.downloadHandler = new DownloadHandlerBuffer();

        var request = www.SendWebRequest();

        while (!request.isDone)
        {
            Debug.Log(request.progress);

            var rect = dlImage.GetComponent<RectTransform>();

            var pos = rect.localPosition;
            var size = rect.sizeDelta;

            pos.x = 243 + (request.progress * 76) / 2;
            size.x = request.progress * 76;

            rect.localPosition = pos;
            rect.sizeDelta = size;

            yield return new WaitForFixedUpdate();
        }

        if (www.isNetworkError || www.isHttpError)
        {
            dlImage.GetComponent<Image>().color = Color.red;
            Debug.Log(www.error);
        }
        else
        {
            var savePath = $"{Application.persistentDataPath}/{beatMapBeatmapsetId}.zip";
            var data = www.downloadHandler.data;

            Debug.Log(data.Length);

            File.WriteAllBytes(savePath, data);

            try
            {
                ZipFile.ExtractToDirectory(savePath, $"{Application.persistentDataPath}/{beatMapBeatmapsetId}");
            }
            catch (IOException e)
            {
                Debug.Log("Error : ZIP EXTRACT MAIN METHOD FAILED");

                using (var zipData = new WebClient().OpenRead($"file://{savePath}"))
                {
                    Debug.Log("Trying to use another system...");
                    Directory.CreateDirectory($"{Application.persistentDataPath}/{beatMapBeatmapsetId}");
                    
                    using(var zipInputStream = new ZipInputStream(zipData))
                    {
                        while (zipInputStream.GetNextEntry() is ZipEntry zipEntry)
                        {
                            var entryFileName = zipEntry.Name;

                            var buffer = new byte[4096];

                            var fullZipToPath = Path.Combine($"{Application.persistentDataPath}/{beatMapBeatmapsetId}", entryFileName);
                            var directoryName = Path.GetDirectoryName(fullZipToPath);
                            if (directoryName.Length > 0)
                                Directory.CreateDirectory(directoryName);

                            if (Path.GetFileName(fullZipToPath).Length == 0)
                            {
                                continue;
                            }

                            using (var streamWriter = File.Create(fullZipToPath))
                            {
                                StreamUtils.Copy(zipInputStream, streamWriter, buffer);
                            }
                        }
                    }
                }
            }

            File.Delete(savePath);

            var files = System.IO.Directory.GetFiles($"{Application.persistentDataPath}/{beatMapBeatmapsetId}",
                "*.osu");
            foreach (var file in files)
            {
                File.Move(file, Path.ChangeExtension(file, ".txt"));
            }
            
            var files2 = System.IO.Directory.GetFiles($"{Application.persistentDataPath}/{beatMapBeatmapsetId}",
                "*.mp3");
            foreach (var file in files2)
            {
                var spath = file.Split(Path.DirectorySeparatorChar);
                
                var newPath = "";
                for (var i = 0; i < spath.Length - 1; i++) {
                    newPath += spath[i] + Path.DirectorySeparatorChar;
                }
                
                File.Move(file, newPath + "audio.mp3");
            }

            bmPane.transform.GetChild(7).GetComponent<Button>().image.sprite = playSprite;
            bmPane.transform.GetChild(5).GetComponent<Image>().sprite = playSprite;

            var rect = dlImage.GetComponent<RectTransform>();

            var pos = rect.localPosition;
            var size = rect.sizeDelta;

            pos.x = 283;
            size.x = 50;
            size.y = 50;

            rect.localPosition = pos;
            rect.sizeDelta = size;

            bmPane.transform.GetChild(7).GetComponent<Button>().onClick.RemoveAllListeners();

            bmPane.transform.GetChild(7).GetComponent<Button>().onClick.AddListener(() =>
            {
                Debug.Log("0.1");
                launchPlay($"{Application.persistentDataPath}/{beatMapBeatmapsetId}");
                Debug.Log("0.2");
            });
        }
    }

    private static void launchPlay(string path)
    {
        SdlPane.gameObject.SetActive(false);
        SplayPane.gameObject.SetActive(true);

        for (var i = 0; i < SplayContentPane.childCount; i++)
        {
            Destroy(SplayContentPane.GetChild(i).gameObject);
        }

        var difficulties = Directory.GetFiles(path, "*.txt");
        var difficultyName = difficulties.Select(s => s.Contains("[") ? s.Split('[')[1].Split(']')[0] : s).ToList();

        var position = new Vector3(0, -60, 0);

        for (var i = 0; i < difficulties.Length; i++)
        {
            var bmPane = Instantiate(SplayPanePrefab, SplayContentPane, true);

            bmPane.transform.GetChild(0).GetComponent<Text>().text = difficultyName[i];
            bmPane.transform.localPosition = position;

            position.y -= 130;

            var i1 = i;
            bmPane.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(() =>
                {Debug.Log("0.3");
                    Play(difficulties[i1]);
                });
        }

        var pos = SplayContentPane.GetComponent<RectTransform>().position;
        pos.y = -99999;
        SplayContentPane.GetComponent<RectTransform>().position = pos;

        SplayPane.GetChild(2).GetComponent<Text>().text = difficulties[0].Contains('[') ? difficulties[0].Split('[')[0].Replace(path, "").Substring(1).Split('-')[1].Split('(')[0] : difficulties[0];
        SplayPane.GetChild(1).GetComponent<Button>().onClick.AddListener(() =>
        {
            SplayPane.gameObject.SetActive(false);
            SdlPane.gameObject.SetActive(true);
        });

        GameHandler.path = path;
        //GameHandler.mainMusic = ;
    }

    private static void Play(string path)
    {
        Debug.Log("0.4");
        GameHandler.path = path;
        Debug.Log("0.5");
        SceneManager.LoadScene("TEST");
    }

    private static IEnumerator SetImage(int mapId, Image image)
    {
        var request = UnityWebRequestTexture.GetTexture($"https://assets.ppy.sh/beatmaps/{mapId}/covers/list@2x.jpg");
        yield return request.SendWebRequest();

        while (!request.isDone)
        {
            yield return new WaitForFixedUpdate();
        }

        var texture = DownloadHandlerTexture.GetContent(request);

        image.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
    }
}
