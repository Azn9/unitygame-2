﻿using System;
using System.Linq;
using TEST;
using UnityEngine;
using UnityEngine.Serialization;

public class OutputVolume : MonoBehaviour
{
    public enum SourceType
    {
        AudioSource,
        AudioListener,
        Custom
    }

    public enum OutputType
    {
        PrefabBar,
        ObjectPosition,
        ObjectRotation,
        ObjectScale
    }


    #region SAMPLING PROPERTIES

    /// <summary>
    /// Enables or disables the processing and display of volume data. 
    /// </summary>
    [Tooltip("Enables or disables the processing and display of volume data.")]
    public bool isEnabled = true;

    /// <summary>
    /// The type of source for volume data.
    /// </summary>
    [Tooltip("The type of source for volume data.")]
    public SourceType sourceType = SourceType.AudioSource;

    /// <summary>
    /// The AudioSource to take data from. Can be empty if sourceType is not AudioSource.
    /// </summary>
    [Tooltip("The AudioSource to take data from.")]
    public AudioSource audioSource;

    /// <summary>
    /// The number of samples to use when sampling. Must be a power of two.
    /// </summary>
    [Tooltip("The number of samples to use when sampling. Must be a power of two.")]
    public int sampleAmount = 256;

    /// <summary>
    /// The audio channel to take data from when sampling.
    /// </summary>
    [Tooltip("The audio channel to take data from when sampling.")]
    public int channel;

    /// <summary>
    /// The amount of dampening used when the new scale is higher than the bar's existing scale. Must be between 0 (slowest) and 1 (fastest).
    /// </summary>
    [Range(0, 1)] [Tooltip("The amount of dampening used when the new scale is higher than the bar's existing scale.")]
    public float attackDamp = .75f;

    /// <summary>
    /// The amount of dampening used when the new scale is lower than the bar's existing scale. Must be between 0 (slowest) and 1 (fastest).
    /// </summary>
    [Range(0, 1)] [Tooltip("The amount of dampening used when the new scale is lower than the bar's existing scale.")]
    public float decayDamp = .25f;

    #endregion

    #region OUTPUT PROPERTIES

    /// <summary>
    /// How the volume data should be presented to the user.
    /// </summary>
    [Tooltip("How the volume data should be presented to the user.")]
    public OutputType outputType = OutputType.PrefabBar;

    /// <summary>
    /// A multiplier / mask to use when object position or rotation is used.
    /// </summary>
    [Tooltip(
        "A multiplier / mask for positioning or rotating. The volume data is multiplied by this vector, so 0 will mask that dimension out.")]
    public Vector3 valueMultiplier = new Vector3(0, 0, -90);

    /// <summary>
    /// The minimum scale when object scaling is used.
    /// </summary>
    [Tooltip("The scale used when output volume is lowest (0).")]
    public float outputScaleMin = 1;

    /// <summary>
    /// The maximum scale when object scaling is used.
    /// </summary>
    [Tooltip("The scale used when output volume is highest (1).")]
    public float outputScaleMax = 1.02f;

    /// <summary>
    /// The prefab of bar to use when building.
    /// Refer to the documentation to use a custom prefab.
    /// </summary>
    [Tooltip(
        "The prefab of bar to use. Use a prefab from SimpleSpectrum/Bar Prefabs or refer to the documentation to use a custom prefab.")]
    public GameObject prefab;

    /// <summary>
    /// Determines whether to scale the bar prefab (i.e. disable for just colouring).
    /// </summary>
    [Tooltip("Determines whether to scale the bar prefab (i.e. disable for just colouring).")]
    public bool scalePrefab = true;

    /// <summary>
    /// Determines whether to apply a color gradient on the bar.
    /// </summary>
    [Tooltip("Determines whether to apply a color gradient on the bar.")]
    public bool useColorGradient;

    /// <summary>
    /// The minimum (low value) color when outputType is PrefabBar.
    /// </summary>
    [Tooltip("The minimum (low value) color.")]
    public Color MinColor = Color.black;

    /// <summary>
    /// The maximum (high value) color when outputType is PrefabBar.
    /// </summary>
    [Tooltip("The maximum (high value) color.")]
    public Color MaxColor = Color.white;
    
    [Tooltip("The maximum (high value) color.")]
    public Color TransparentColor = Color.clear;

    /// <summary>
    /// The curve that determines the interpolation between colorMin and colorMax, when outputType is PrefabBar.
    /// </summary>
    [Tooltip("The curve that determines the interpolation between Color Min and Color Max")]
    public AnimationCurve colorCurve;

    /// <summary>
    /// The amount of dampening used when the new color value is higher than the existing color value. Must be between 0 (slowest) and 1 (fastest).
    /// </summary>
    [Range(0, 1)]
    [Tooltip("The amount of dampening used when the new color value is higher than the existing color value.")]
    public float colorAttackDamp = 1;

    /// <summary>
    /// The amount of dampening used when the new color value is lower than the existing color value. Must be between 0 (slowest) and 1 (fastest).
    /// </summary>
    [Range(0, 1)]
    [Tooltip("The amount of dampening used when the new color value is lower than the existing color value.")]
    public float colorDecayDamp = 1;

    #endregion

    public static bool kiai = false;

    //float[] samples;

    private GameObject _bar;

    private float _newValue;
    private float _oldScale = 1;
    private float _oldColorVal;
    private Material _mat;

    private static readonly int Color1 = Shader.PropertyToID("_Color1");
    private static readonly int Color2 = Shader.PropertyToID("_Color2");
    private MeshRenderer _meshRenderer;

    private void Start()
    {
        if (outputType != OutputType.PrefabBar) return;

        _bar = prefab;

        _meshRenderer = _bar.GetComponent<MeshRenderer>();

        _mat = _meshRenderer.material;
        
        _mat.color = MinColor;

        _bar.GetComponent<MeshRenderer>().material = _mat;
    }

    public static float NewScale;
    
    private bool kiaiEnable = false;
    private Color lowColor;

    private void Update()
    {
        if (kiai && !kiaiEnable) {
            lowColor = MinColor;
            MinColor = MaxColor;
            MaxColor = Color.white;
            attackDamp = 1f;

            kiaiEnable = true;
        }

        if (!kiai && kiaiEnable) {
            attackDamp = 0.03f;

            MaxColor = MinColor;
            MinColor = lowColor;
            kiaiEnable = false;
        }


        if (isEnabled && sourceType != SourceType.Custom)
        {
            _newValue = sourceType == SourceType.AudioListener
                ? GetRms(sampleAmount, channel)
                : GetRms(audioSource, sampleAmount, channel);
        }

        NewScale = _newValue > _oldScale
            ? Mathf.Lerp(_oldScale, _newValue, attackDamp)
            : Mathf.Lerp(_oldScale, _newValue, decayDamp);

        if (NewScale < 0.32f)
            NewScale = 0.32f;
        
        if (NewScale > 0.4f)
            NewScale = 0.4f;
        
        //Debug.Log(NewScale);

        switch (outputType)
        {
            case OutputType.PrefabBar:
                if (scalePrefab)
                {
                    var transformLocalScale = _bar.transform.localScale;
                    transformLocalScale.x -= _oldScale;
                    transformLocalScale.y -= _oldScale;
                    
                    transformLocalScale.x += NewScale;
                    transformLocalScale.y += NewScale;
                    _bar.transform.localScale = transformLocalScale;
                }
                
                _oldScale = NewScale;

                if (useColorGradient)
                {
                    var newColorVal = colorCurve.Evaluate(NewScale);
                    var color = Color.Lerp(MinColor, MaxColor,
                        newColorVal > _oldColorVal ? colorAttackDamp : colorDecayDamp);

                    _mat.color = color;

                    _oldColorVal = newColorVal;

                    _meshRenderer.material = _mat;
                }

                break;

            case OutputType.ObjectPosition:
                transform.localPosition = valueMultiplier * NewScale;
                break;

            case OutputType.ObjectRotation:
                transform.localEulerAngles = valueMultiplier * NewScale;
                break;

            case OutputType.ObjectScale:
                var s = Mathf.Lerp(outputScaleMin, outputScaleMax, NewScale);
                transform.localScale = new Vector3(s, s, s);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    /// <summary>
    /// Returns the current output volume of the specified AudioSource, using the RMS method.
    /// </summary>
    /// <param name="aSource">The AudioSource to reference.</param>
    /// <param name="sampleSize">The number of samples to take, as a power of two. Higher values mean more precise volume.</param>
    /// <param name="channelUsed">The audio channel to take data from.</param>
    private static float GetRms(AudioSource aSource, int sampleSize, int channelUsed = 0)
    {
        sampleSize = Mathf.ClosestPowerOfTwo(sampleSize);
        var outputSamples = new float[sampleSize];
        aSource.GetOutputData(outputSamples, channelUsed);

        var rms = outputSamples.Sum(f => f * f);

        return Mathf.Sqrt(rms / outputSamples.Length); //mean and root
    }

    /// <summary>
    /// Returns the current output volume of the scene's AudioListener, using the RMS method.
    /// </summary>
    /// <param name="sampleSize">The number of samples to take, as a power of two. Higher values mean more precise volume.</param>
    /// <param name="channelUsed">The audio channel to take data from.</param>
    private static float GetRms(int sampleSize, int channelUsed = 0)
    {
        sampleSize = Mathf.ClosestPowerOfTwo(sampleSize);
        var outputSamples = new float[sampleSize];
        AudioListener.GetOutputData(outputSamples, channelUsed);

        var rms = outputSamples.Sum(f => f * f);

        return Mathf.Sqrt(rms / outputSamples.Length); //mean and root
    }
}