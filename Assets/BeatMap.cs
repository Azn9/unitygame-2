﻿using System;
namespace test
{
    public class BeatMap
    {
        public string artist { get; set; }
        public int beatmapset_id { get; set; }
        public string creator { get; set; }
        public int ranked { get; set; }
        public string title { get; set; }
    }
}
