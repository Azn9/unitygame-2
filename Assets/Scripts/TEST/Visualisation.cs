﻿using UnityEngine;

namespace TEST
{
    [RequireComponent(typeof(AudioSource))]
    public class Visualisation : MonoBehaviour
    {

        private AudioSource _audioSource;
        public static float[] samples = new float[512];

        private void Start()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        private void Update()
        {
            GetSpectrumAudioSource();
        }

        private void GetSpectrumAudioSource()
        {
            _audioSource.GetSpectrumData(samples, 0, FFTWindow.Blackman);
        }
    }
}
