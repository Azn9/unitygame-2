﻿using UnityEngine;

public class Mover : MonoBehaviour
{
    public GameObject obj;
    public static float _angle;
    public Material wrongMateriale;

    public static Material wrongMaterial;

    public GameObject circle;

    private void Start() {
        wrongMaterial = wrongMateriale;
    }

    private void Update()
    {
        var mousePos = Input.mousePosition;

        var width = Display.displays[0].renderingWidth;
        var height = Display.displays[0].renderingHeight;

        mousePos.x = (float) (mousePos.x - (double) width / 2);
        mousePos.y = (float) (mousePos.y - (double) height / 2);

        //Debug.LogFormat("X : {0} | Y : {1} | Z : {2}", mousePos.x, mousePos.y, mousePos.z);

        mousePos = Vector3.Normalize(mousePos);
        var center = Vector3.zero;

        var result = center + mousePos;

        var angleX = Mathf.Acos(result.x);
        var angleY = Mathf.Asin(result.y);

        if (angleY > 0)
            _angle = -angleX;
        else
            _angle = angleX;

        _angle = _angle * Mathf.Rad2Deg;

        obj.transform.localEulerAngles = new Vector3(_angle, 90, 0);

        result *= circle.transform.localScale.x * 7.8f;
        result.z = 3.5f;

        obj.transform.position = result;
    }
}