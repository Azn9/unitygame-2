﻿using UnityEngine;

namespace TEST
{
    public class CubesCreator : MonoBehaviour
    {
        public GameObject originalCube;
        public GameObject[] createdCubes = new GameObject[512];

        private readonly Vector3[] _originalPositions = new Vector3[512];

        private void Start()
        {
            Application.targetFrameRate = -1;
            
            for (var i = 0; i < 512; i++)
            {
                var instance = Instantiate(originalCube, transform, true);
                transform.eulerAngles = new Vector3(0, 0, 0.703125f * i);
                instance.transform.position = Vector3.up * 2;
                createdCubes[i] = instance;
                instance.name = "Instace-" + i;
                _originalPositions[i] = instance.transform.localPosition;
            }
        }

        private void Update()
        {
            for (var i = 1; i < 512; i++)
            {
                if (createdCubes[i].Equals(null)) continue;

                var size = Visualisation.samples[i] * 5 * ((float) 22050 / i);
                createdCubes[i].transform.localScale = new Vector3(0.05f, size, 0.05f);

                var transformLocalPosition = _originalPositions[i];
                transformLocalPosition.y += size / 2;
                createdCubes[i].transform.localPosition = transformLocalPosition;
            }
        }
    }
}