using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Threading;
using UnityEngine.Networking;
using UnityEngine.Serialization;

public class GameHandler : MonoBehaviour
{
    // ----------------------------------------------------------------------------

    [Header("Objects")] public GameObject circle; // Circle Object

    [Header("Map")]
    //public DefaultAsset mapFile; // Map file (.osu format), attach from editor
    public static string path;
    //public static TextAsset textAsset;

    public static AudioClip mainMusic; // Music file, attach from editor
    public AudioClip hitSound; // Hit sound
    public GameObject cursor;

    public GameObject circleA;

    // ----------------------------------------------------------------------------

    public static float Timer; // Main song timer
    public const int ApprRate = 600; // Approach rate (in ms)
    private int _delayPos; // Delay song position
    private static int _objCount; // Spawned objects counter

    private List<GameObject> _circleList; // Circles List
    private static string[] _lineParams; // Object Parameters

    // Audio stuff
    private AudioSource _music;

    // Other stuff
    private Vector3 _mousePosition;
    private Ray _mainRay;
    private RaycastHit _mainHit;

    private void Start()
    {
        Debug.Log("1");

        _music = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>();
        _circleList = new List<GameObject>();
        //var path = AssetDatabase.GetAssetPath(mapFile);
        Debug.Log(path);

        Debug.Log("2");

        var file = new StreamReader(path);
        var lines = new List<string>();
        var line = "";

        var i = 0;

        Debug.Log("3");

        while ((line = file.ReadLine()) != null && i < 2000)
        {
            lines.Add(line);

            i++;
            if (i > 2000)
                break;
        }

        Debug.Log("4");

        if (i >= 2000)
        {
            Debug.Log("ckc");

            return;
        }

        ReadCircles(lines); //textAsset.text);//Resources.Load<Object>("osu.osu"));
    }

    // MAP READER

    private List<float> kiaiStartTimes = new List<float>();
    private List<float> kiaiStopTimes = new List<float>();
    private bool kiaiE = false;

    private bool audioDL = false;

    private IEnumerator PlayTask(string path)
    {
        Debug.Log(path);

        var request = UnityWebRequestMultimedia.GetAudioClip(path, AudioType.MPEG);

        yield return request.SendWebRequest();

        while (!request.isDone)
        {
            yield return new WaitForFixedUpdate();
        }

        mainMusic = DownloadHandlerAudioClip.GetContent(request);

        audioDL = true;
    }

    private void ReadCircles(List<string> linesArr)
    {
        Debug.Log("= ReadCircles =");

        var lines = linesArr.ToArray(); //file.Split('\n');

        //var reader = new StreamReader(path);
        var line = "";


        var index = 0;
        while (true && index < lines.Length)
        {
            line = lines[index];

            if (line.Contains("AudioFilename"))
            {
                var audioName = line.Split(':')[1];
                if (audioName[0] == ' ')
                    audioName = audioName.Substring(1);

                if (!(path[path.Length - 1] == '\\' || path[path.Length - 1] == '/'))
                    path += Path.DirectorySeparatorChar;

                var spath = path.Split(Path.DirectorySeparatorChar);

                var newPath = "";
                for (var i = 0; i < spath.Length - 2; i++)
                {
                    newPath += spath[i] + Path.DirectorySeparatorChar;
                }

                newPath += "audio.mp3";

                Debug.Log($"Loading audio file...");

                //StartCoroutine(PlayTask($"file://{newPath}"));

                var request = UnityWebRequestMultimedia.GetAudioClip($"file://{newPath}", AudioType.MPEG);

                request.SendWebRequest();

                var i2 = 0;

                while (!request.isDone && i2 < 1000)
                {
                    i2++;
                    
                    Thread.Sleep(10);
                }

                if (i2 == 1000)
                {
                    Debug.Log("Err");
                    return;
                }

                mainMusic = DownloadHandlerAudioClip.GetContent(request);

                index++;

                continue;
            }

            if (line.Contains("[TimingPoints]"))
                break;
            index++;
        }

        if (index >= lines.Length)
        {
            Debug.Log("ckc2");

            return;
        }

        line = "";

        while (!line.Contains("["))
        {
            index++;

            if (index == lines.Length)
                break;

            line = lines[index];

            if (String.IsNullOrEmpty(line) || String.IsNullOrWhiteSpace(line))
                break;

            _lineParams = line.Split(',');

            if (_lineParams.Length == 0)
                break;
            else
            {
                //Debug.Log(line + " | " + _lineParams[0]);

                var time = float.Parse(_lineParams[0]);
                var kiai = _lineParams[7].Contains("1");

                if (kiai && !kiaiE)
                {
                    //Debug.Log("Added " + time + " as kiai mode start time !");
                    kiaiE = true;
                    kiaiStartTimes.Add(time);
                }

                if (!kiai && kiaiE)
                {
                    //Debug.Log("Added " + time + " as kiai mode stop time !");
                    kiaiE = false;
                    kiaiStopTimes.Add(time);
                }
            }
        }

        // Skip to [HitObjects] part
        while (index < lines.Length)
        {
            line = lines[index];
            if (line.Contains("[HitObjects]"))
                break;
            index++;
        }

        if (index >= lines.Length)
        {
            Debug.Log("ckc3");

            return;
        }

        var totalLines = 0;

        /*// Count all lines
        while (!reader.EndOfStream)
        {
            reader.ReadLine();
            totalLines++;
        }

        reader.Close();

        reader = new StreamReader(path);

        // Skip to [HitObjects] part again
        while (true)
        {
            if (reader.ReadLine() == "[HitObjects]")
                break;
        }

        // Some crazy Z axis modifications for sorting
        var totalLinesStr = "0,";
        Debug.Log(totalLines.ToString().Length);
        for (var i = 3; i > totalLines.ToString().Length; i--)
            totalLinesStr += "0";
        totalLinesStr += totalLines.ToString();

        Debug.Log(totalLinesStr);

        var zIndex = -float.Parse(totalLinesStr);*/

        while (index < lines.Length)
        {
            // Uncomment to skip sliders
            /*while (true)
            {
                line = reader.ReadLine();
                if (line != null)
                {
                    if (!line.Contains("|"))
                        break;
                }
                else
                    break;
            }*/
            index++;

            if (index >= lines.Length)
                break;

            line = lines[index];

            if (string.IsNullOrEmpty(line) || string.IsNullOrWhiteSpace(line))
                break;

            _lineParams = line.Split(','); // Line parameters (X&Y axis, time position)

            // Sorting configuration

            float angle = UnityEngine.Random.Range(0, 8) * 45;
            var x = Mathf.Cos(angle) / 3;
            var y = Mathf.Sin(angle) / 3;

            var angleRot = angle + 135;


            var circleObject = Instantiate(circle,
                new Vector3(x, y, (float) (6.5F + (double) 16 * int.Parse(_lineParams[2]) / 1000)),
                Quaternion.Euler(0, 0, angleRot));
            circleObject.transform.parent = transform;
            /*SetLayerRecursively(CircleObject);
            CircleObject.GetComponent<Circle>().Fore.sortingOrder = ForeOrder;
            CircleObject.GetComponent<Circle>().Back.sortingOrder = BackOrder;
            CircleObject.GetComponent<Circle>().Appr.sortingOrder = ApproachOrder;
            CircleObject.GetComponent<Circle>().Slider = line.Contains("P|") || line.Contains("L|") || line.Contains("B|");

            CircleObject.transform.localPosition += new Vector3((float) CircleObject.transform.localPosition.x, (float) CircleObject.transform.localPosition.y, (float) Z_Index);
            CircleObject.transform.SetAsFirstSibling();
            ForeOrder--; BackOrder--; ApproachOrder--; Z_Index += 0.01f;

            int FlipY = 384 - int.Parse(LineParams[1]); // Flip Y axis

            int AdjustedX = Mathf.RoundToInt(Screen.height * 1.333333f); // Aspect Ratio

            // Padding
            float Slices = 8f;
            float PaddingX = AdjustedX / Slices;
            float PaddingY = Screen.height / Slices;

            // Resolution set
            float NewRangeX = ((AdjustedX - PaddingX) - PaddingX);
            float NewValueX = ((int.Parse(LineParams[0]) * NewRangeX) / 512f) + PaddingX + ((Screen.width - AdjustedX) / 2f);
            float NewRangeY = Screen.height;
            float NewValueY = ((FlipY * NewRangeY) / 512f) + PaddingY;

            Vector3 MainPos = new Vector3(0,0,0); //MainCamera.ScreenToWorldPoint(new Vector3 (NewValueX, NewValueY, 0)); // Convert from screen position to world position
            Circle MainCircle = CircleObject.GetComponent<Circle>();

            MainCircle.Set(MainPos.x, MainPos.y, CircleObject.transform.position.z, int.Parse(LineParams[2]) - ApprRate);

            */
            _circleList.Add(circleObject);
        }

        GameStart();
    }

    // END MAP READER

    private void GameStart()
    {
        Debug.Log(audioDL);
        Debug.Log($"Circles: {_circleList.Count}");

        transform.localScale = new Vector3(0.3f, 0.3f, 1); //Cursor.lockState = CursorLockMode.Locked;

        Application.targetFrameRate = -1; // Unlimited framerate

        _music.clip = mainMusic;
        _music.Play();

        StartCoroutine(UpdateRoutine()); // Using coroutine instead of Update()
    }


    private bool _slider;
    private bool _hit;

    private IEnumerator UpdateRoutine()
    {
        while (true)
        {
            Timer = _music.time * 1000; // Convert timer

            //Debug.Log(Timer);

            if (kiaiStartTimes.Count > 0)
                if (Timer >= kiaiStartTimes[0])
                {
                    OutputVolume.kiai = true;
                    SimpleSpectrum.kiai = true;

                    kiaiStartTimes.RemoveAt(0);
                    Debug.Log("<color=lime>Entered kiai mode !</color>");
                }

            if (kiaiStopTimes.Count > 0)
                if (Timer >= kiaiStopTimes[0])
                {
                    OutputVolume.kiai = false;
                    SimpleSpectrum.kiai = false;

                    kiaiStopTimes.RemoveAt(0);
                    Debug.Log("<color=red>Exited kiai mode !</color>");
                }

            yield return null;
        }
    }
}