using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

public class Circle : MonoBehaviour
{
    // Circle parameters
    private float _posX;
    private float _posY;
    private float _posZ;
    [FormerlySerializedAs("PosA")] [HideInInspector] public int posA;

    private Color _mainColor, _mainColor1, _mainColor2; // Circle sprites color
    [FormerlySerializedAs("MainApproach")] public GameObject mainApproach; // Circle objects

    // Checker stuff
    private bool _removeNow;
    public bool _gotIt;
    [FormerlySerializedAs("Slider")] public bool slider;
    private Rigidbody _rigidbody;

    // Spawning the circle
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void Spawn()
    {
        gameObject.transform.position = new Vector3(_posX, _posY, _posZ);
        enabled = true;
    }

    private void Update()
    {
        _rigidbody.velocity = new Vector3(0, 0, -16);

        if (transform.position.z < 3.5 && !_gotIt) {
            var angle = transform.localEulerAngles.z;
            angle = angle + 45;

            var _angle = ((-Mover._angle) + 180) - 90;
            if (_angle < 0)
                _angle = (_angle + 135) + 225;

            //Debug.Log(angle + " | " + _angle);

            if (angle - 45 < 0 || angle + 45 > 360) {
                if (!((_angle < 360 && _angle > 360 + (angle - 45)) || (_angle > 0 && _angle < angle + 45 - 360)))
                    GetComponent<MeshRenderer>().material = Mover.wrongMaterial;
            } else {
                if (!(_angle > angle -45 && _angle < angle + 45))
                    GetComponent<MeshRenderer>().material = Mover.wrongMaterial;
            }

            _gotIt = true;
        }
    }
}
