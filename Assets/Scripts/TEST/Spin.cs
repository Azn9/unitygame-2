﻿using UnityEngine;

public class Spin : MonoBehaviour
{
    private void Update()
    {
        var speed = 500f + 1000 * OutputVolume.NewScale;

        transform.Rotate(Vector3.forward, - speed * Time.deltaTime);
    }
}
