﻿// returns gradient Texture2D (size=256x1)

using UnityEngine;

namespace TEST
{
    public static class GradientTextureMaker
    {
        private const int Width = 256;
        private const int Height = 1;

        public static Texture2D Create(Color[] colors, TextureWrapMode textureWrapMode = TextureWrapMode.Clamp,
            FilterMode filterMode = FilterMode.Point, bool isLinear = false, bool hasMipMap = false)
        {
            if (colors == null || colors.Length == 0)
            {
                Debug.LogError("No colors assigned");
                return null;
            }

            var length = colors.Length;
            if (colors.Length > 8)
            {
                Debug.LogWarning("Too many colors! maximum is 8, assigned: " + colors.Length);
                length = 8;
            }

            // build gradient from colors
            var colorKeys = new GradientColorKey[length];
            var alphaKeys = new GradientAlphaKey[length];

            var steps = length - 1f;
            for (var i = 0; i < length; i++)
            {
                var step = i / steps;
                colorKeys[i].color = colors[i];
                colorKeys[i].time = step;
                alphaKeys[i].alpha = colors[i].a;
                alphaKeys[i].time = step;
            }

            // create gradient
            var gradient = new Gradient();
            gradient.SetKeys(colorKeys, alphaKeys);

            // create texture
            var outputTex = new Texture2D(Width, Height, TextureFormat.ARGB32, false, isLinear);
            outputTex.wrapMode = textureWrapMode;
            outputTex.filterMode = filterMode;

            // draw texture
            for (var i = 0; i < Width; i++)
            {
                outputTex.SetPixel(i, 0, gradient.Evaluate(i / (float) Width));
            }

            outputTex.Apply(false);

            return outputTex;
        } // BuildGradientTexture
    } // class
} // namespcae