﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MusicHandler : MonoBehaviour
{
    public Button button;
    public AudioSource audioSource;

    public Sprite play;
    public Sprite pause;
    public Sprite loading;

    public int beatmapId;

    // Start is called before the first frame update
    private void Start()
    {
        button.onClick.AddListener(Play);
    }

    private void Play()
    {
        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        else
        {
            StartCoroutine(PlayTask());
        }
    }

    private IEnumerator SpinRoutine()
    {
        var transform1 = button.transform;
        while (true)
        {
            yield return new WaitForSeconds(0.05f);
            
            var rotation = transform1.localRotation;
            rotation.z += 0.1f;
            transform1.localRotation = rotation;
        }
    }
 
    private IEnumerator PlayTask()
    {
        Debug.Log(beatmapId);
        
        var request = UnityWebRequestMultimedia.GetAudioClip($"https://b.ppy.sh/preview/{beatmapId}.mp3", AudioType.MPEG);
        button.image.sprite = loading;

        var routine = StartCoroutine(SpinRoutine());
        
        yield return request.SendWebRequest();
        
        while (!request.isDone)
        {
            yield return new WaitForFixedUpdate();
        }
        
        StopCoroutine(routine);

        button.image.sprite = pause;
        var transform1 = button.transform;
        var rotation = transform1.localRotation;
        rotation.z = 0;
        transform1.localRotation = rotation;

        var audioClip = DownloadHandlerAudioClip.GetContent(request);

        audioSource.clip = audioClip;
        audioSource.Play();
        
        while (audioSource.isPlaying)
        {
            yield return new WaitForSeconds(0.1f);
        }

        button.image.sprite = play;

        yield return null;
    }
}